/**
 * 
 */
package us.wishcoder.marketplace.server.messages;

import java.io.Serializable;

/**
 * @author ajaysingh
 *
 */
public class LoginRequest implements Serializable {
	private static final long serialVersionUID = 4195589838411082551L;

	
	private final String userId;
	private final String userPwd;
	
	/**
	 * @param userId
	 * @param userPwd
	 */
	public LoginRequest(String userId, String userPwd) {
		this.userId = userId;
		this.userPwd = userPwd;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @return the userPwd
	 */
	public String getUserPwd() {
		return userPwd;
	}


	@Override
	public String toString() {
		return "LoginRequest [userId=" + userId + ", userPwd=" + userPwd + "]";
	}
}
