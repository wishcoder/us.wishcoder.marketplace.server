/**
 * 
 */
package us.wishcoder.marketplace.server.messages;

import java.io.Serializable;

/**
 * @author ajaysingh
 *
 */
public class UserPermission implements Serializable {
	
	private static final long serialVersionUID = 3384356431717757533L;
	private final String userId;
	private final int maxBuyQty;
	private final int maxBuyPrice;
	
	/**
	 * @param userId
	 * @param maxBuyQty
	 * @param maxBuyPrice
	 */
	public UserPermission(String userId, int maxBuyQty, int maxBuyPrice) {
		this.userId = userId;
		this.maxBuyQty = maxBuyQty;
		this.maxBuyPrice = maxBuyPrice;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @return the maxBuyQty
	 */
	public int getMaxBuyQty() {
		return maxBuyQty;
	}

	/**
	 * @return the maxBuyPrice
	 */
	public int getMaxBuyPrice() {
		return maxBuyPrice;
	}

	@Override
	public String toString() {
		return "UserPermission [userId=" + userId + ", maxBuyQty=" + maxBuyQty
				+ ", maxBuyPrice=" + maxBuyPrice + "]";
	}
}
