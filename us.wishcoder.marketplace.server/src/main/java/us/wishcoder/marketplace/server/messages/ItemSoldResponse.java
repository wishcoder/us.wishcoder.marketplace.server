/**
 * 
 */
package us.wishcoder.marketplace.server.messages;

import java.io.Serializable;

/**
 * @author ajaysingh
 *
 */
public class ItemSoldResponse implements Serializable {
	private static final long serialVersionUID = -121467171673239974L;

	private final String userIdSeller;
	private final String userIdBuyer;
	private final Item item;
	
	/**
	 * @param userIdSeller
	 * @param userIdBuyer
	 * @param item
	 */
	public ItemSoldResponse(String userIdSeller, String userIdBuyer, Item item) {
		this.userIdSeller = userIdSeller;
		this.userIdBuyer = userIdBuyer;
		this.item = item;
	}

	/**
	 * @return the userIdSeller
	 */
	public String getUserIdSeller() {
		return userIdSeller;
	}

	/**
	 * @return the userIdBuyer
	 */
	public String getUserIdBuyer() {
		return userIdBuyer;
	}

	/**
	 * @return the item
	 */
	public Item getItem() {
		return item;
	}

	@Override
	public String toString() {
		return "ItemSoldResponse [userIdSeller=" + userIdSeller
				+ ", userIdBuyer=" + userIdBuyer + ", item=" + item + "]";
	}
}
