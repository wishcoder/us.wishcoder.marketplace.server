/**
 * 
 */
package us.wishcoder.marketplace.server.messages;

import java.io.Serializable;

/**
 * @author ajaysingh
 *
 */
public class Item implements Serializable {	
	private static final long serialVersionUID = 8254335790438974904L;

	public static final String ITEM_TV = "tv";
	public static final String ITEM_PHONE = "phone";
	public static final String ITEM_PEN = "pen";
	public static final String ITEM_JEAN = "jean";

	
	private final String itemName;
	private final int itemMinPrice;
	private final int itemSalePrice;
	private int itemQty;
	private String sellerId;
	
	/**
	 * @param itemName
	 * @param itemMinPrice
	 * @param itemSalePrice
	 * @param itemQty
	 */
	public Item(String itemName, int itemMinPrice, int itemSalePrice,
			int itemQty) {
		this.itemName = itemName;
		this.itemMinPrice = itemMinPrice;
		this.itemSalePrice = itemSalePrice;
		this.itemQty = itemQty;
		this.sellerId = "MarketplaceServerApp"; // default
	}
	
	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * @return the itemMinPrice
	 */
	public int getItemMinPrice() {
		return itemMinPrice;
	}

	/**
	 * @return the itemSalePrice
	 */
	public int getItemSalePrice() {
		return itemSalePrice;
	}

	/**
	 * @param itemQty the itemQty to set
	 */
	public void setItemQty(int itemQty) {
		this.itemQty = itemQty;
	}

	/**
	 * @return the itemQty
	 */
	public int getItemQty() {
		return itemQty;
	}
	
	/**
	 * decreaseItemQty
	 * @param qty
	 */
	public void decreaseItemQty(int qty){
		itemQty = itemQty - qty;
	}

	/**
	 * @return the sellerId
	 */
	public String getSellerId() {
		return sellerId;
	}

	
	/**
	 * @param sellerId the sellerId to set
	 */
	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}


	@Override
	public String toString() {
		return "Item [itemName=" + itemName + ", itemMinPrice=" + itemMinPrice
				+ ", itemSalePrice=" + itemSalePrice + ", itemQty=" + itemQty
				+ ", sellerId=" + sellerId + "]";
	}
}
