/**
 * 
 */
package us.wishcoder.marketplace.server.messages;

import java.io.Serializable;
import java.util.Map;

/**
 * @author ajaysingh
 * 
 */
public class ItemStatus implements Serializable {
	private static final long serialVersionUID = -9147134927140256760L;
	private final Map<String, Item> items;

	public ItemStatus(Map<String, Item> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		StringBuffer sBuffer = new StringBuffer();

		sBuffer.append("\n**************************************************************************\n");
		sBuffer.append("Marketplace Inventory Status [\n");

		for (Map.Entry<String, Item> itemEntry : items.entrySet()) {
			sBuffer.append("		[" + itemEntry.getKey() + "="
					+ itemEntry.getValue().toString() + "\n");
		}

		sBuffer.append("]\n");
		sBuffer.append("**************************************************************************\n");

		return sBuffer.toString();
	}

}
