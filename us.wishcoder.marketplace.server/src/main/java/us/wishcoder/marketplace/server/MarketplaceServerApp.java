/**
 * 
 */
package us.wishcoder.marketplace.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.wishcoder.marketplace.server.messages.BuyRequest;
import us.wishcoder.marketplace.server.messages.BuyResponse;
import us.wishcoder.marketplace.server.messages.Item;
import us.wishcoder.marketplace.server.messages.ItemSoldResponse;
import us.wishcoder.marketplace.server.messages.ItemStatus;
import us.wishcoder.marketplace.server.messages.LoginRequest;
import us.wishcoder.marketplace.server.messages.LoginResponse;
import us.wishcoder.marketplace.server.messages.LogoutRequest;
import us.wishcoder.marketplace.server.messages.SellRequest;
import us.wishcoder.marketplace.server.messages.SellResponse;
import us.wishcoder.marketplace.server.messages.User;
import us.wishcoder.marketplace.server.messages.UserPermission;

/**
 * @author ajaysingh
 * 
 */
public class MarketplaceServerApp {
	protected final Logger LOG = LoggerFactory.getLogger(MarketplaceServerApp.class);
	
	private static boolean acceptMore = true;

	private ExecutorService executorService;
	private ServerSocket serverSocket;
	private int activeClients;
	private final Map<String, Socket> clientSockets;

	// listed items
	private static final Map<String, Item> items = new HashMap<String, Item>();

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		new MarketplaceServerApp().start();
	}

	/**
	 * MarketplaceServerApp
	 */
	public MarketplaceServerApp() {
		executorService = Executors.newFixedThreadPool(100);
		clientSockets = new HashMap<String, Socket>();
		
		// initialize items for auction
//		Item itemTv = new Item(Item.ITEM_TV, 500, 1000, 10);
//		Item itemPhone = new Item(Item.ITEM_PHONE, 200, 500, 10);
//		Item itemPen = new Item(Item.ITEM_PEN, 5, 10, 100);
//		Item itemJean = new Item(Item.ITEM_JEAN, 25, 50, 100);
//
//		items.put(Item.ITEM_TV, itemTv);
//		items.put(Item.ITEM_PHONE, itemPhone);
//		items.put(Item.ITEM_PEN, itemPen);
//		items.put(Item.ITEM_JEAN, itemJean);

		// print items status
		LOG.info(new ItemStatus(items).toString());
	}

	/**
	 * addClient
	 * @param clientName
	 * @param clientSocket
	 */
	public void addClient(String clientName, Socket clientSocket) {
		if(!this.clientSockets.containsKey(clientName)){
			this.activeClients++;
			this.clientSockets.put(clientName, clientSocket);
		}
	}

	/**
	 * removeClient
	 * @param clientName
	 */
	public void removeClient(String clientName) {
		if(this.clientSockets.containsKey(clientName)){
			this.activeClients--;
			this.clientSockets.remove(clientName);
//			if (this.activeClients == 0) {
//				acceptMore = false;
//				
//				executorService.shutdownNow();
//				System.out
//				.println("No more active clients. MarketplaceServerApp service stopped");
//				System.exit(0);
//			}
		}
	}

	/**
	 * processBuyRequest
	 * 
	 * @param buyRequest
	 * @return BuyResponse
	 * @throws Exception
	 */
	public BuyResponse processBuyRequest(BuyRequest buyRequest)
			throws Exception {
		BuyResponse buyResponse = new BuyResponse(buyRequest);
		buyResponse.setBuyRequestSuccessful(false); // default false

		Item buyItem = items.get(buyRequest.getItemName().toLowerCase());

		// Check if item exist
		if (buyItem == null) {
			buyResponse
					.setErrorCode(BuyResponse.ERROR_CODE.ITEM_DOES_NOT_EXIST);
		} else {
			// check item quantity
			if (buyItem.getItemQty() < buyRequest.getBuyQty()) {
				buyResponse.setErrorCode(BuyResponse.ERROR_CODE.ITEM_SOLD_OUT);
			} else {
				// check minimum item price
				if (buyItem.getItemMinPrice() > buyRequest.getBuyPrice()) {
					buyResponse
							.setErrorCode(BuyResponse.ERROR_CODE.REQUIRE_MIN_BUY_PRICE);
				} else {
					// all criteria met
					// accept buy item request and update Item quantity
					buyItem.decreaseItemQty(buyRequest.getBuyQty());
					buyResponse.setBuyRequestSuccessful(true);
					buyResponse.setItemStatus(new ItemStatus(items));
					buyResponse.setBuyItem(buyItem);
				}
			}
		}

		return buyResponse;
	}

	/**
	 * processSellRequest
	 * 
	 * @param sellRequest
	 * @return SellResponse
	 * @throws Exception
	 */
	public SellResponse processSellRequest(SellRequest sellRequest)
			throws Exception {
		SellResponse sellResponse = new SellResponse(sellRequest);
		sellResponse.setRequestSuccessful(true);

		/**
		 * @param itemName
		 * @param itemMinPrice
		 * @param itemSalePrice
		 * @param itemQty
		 */
		Item sellItem = new Item(sellRequest.getItemName(),
				sellRequest.getItemMinPrice(), sellRequest.getItemPrice(),
				sellRequest.getItemQty());

		sellItem.setSellerId(sellRequest.getUserId());

		items.put(sellItem.getItemName(), sellItem);

		sellResponse.setItemStatus(new ItemStatus(items));

		return sellResponse;
	}

	
	/**
	 * notifySeller
	 * @param buyResponse
	 * @throws Exception
	 */
	public void notifySeller(BuyResponse buyResponse) throws Exception{
		String sellerId = buyResponse.getBuyItem().getSellerId();
		Socket sellerSocket = clientSockets.get(sellerId);
		if(sellerSocket != null){
			Item buyItem = buyResponse.getBuyItem();
			buyItem.setItemQty(buyResponse.getBuyRequest().getBuyQty());
			
			ItemSoldResponse itemSoldResponse = new ItemSoldResponse(sellerId, buyResponse.getBuyRequest().getUserId(), buyResponse.getBuyItem());
			
			ObjectOutputStream oos = new ObjectOutputStream(
					sellerSocket.getOutputStream());
			oos.writeObject(itemSoldResponse);

			LOG.info("Item sold notification sent to client: "
					+ itemSoldResponse.toString());
		}
	}
	
	public void start() throws Exception {
		String serverError = "";
		try {
			serverSocket = new ServerSocket(9090);
			LOG.info("MarketplaceServerApp service started on port: 9090");
			while (acceptMore) {
				if (!acceptMore)
					break;
				Socket clientSocket = serverSocket.accept();
				executorService.submit(new SocketCallable(this, clientSocket));
			}
		} catch (IOException exp) {
			serverError = exp.getMessage();
		} finally {
			try {
				serverSocket.close();
			} catch (Exception e) {
			}
			executorService.shutdownNow();
			LOG.error("MarketplaceServerApp service stopped due to error: "
					+ serverError);
			System.exit(0);
		}
	}

	/**
	 * SocketCallable
	 * 
	 * @author ajaysingh
	 * 
	 */
	static class SocketCallable implements Callable<Object> {
		protected final Logger LOG = LoggerFactory.getLogger(SocketCallable.class);
		
		private MarketplaceServerApp marketplaceServerApp;
		private Socket clientSocket;
		private ObjectInputStream streamIn = null;

		public SocketCallable(MarketplaceServerApp marketplaceServerApp,
				Socket clientSocket) {
			this.marketplaceServerApp = marketplaceServerApp;
			this.clientSocket = clientSocket;
		}

		@Override
		public Object call() throws Exception {
			while (true) {
				try {
					if(!open()){
						return null;
					}
					
					Object clientRequestObj = streamIn.readObject();

					if (clientRequestObj instanceof LoginRequest) {
						processLoginRequest(clientRequestObj);
					} else if (clientRequestObj instanceof BuyRequest) {
						processBuyRequest(clientRequestObj);
					} else if (clientRequestObj instanceof SellRequest) {
						processSellRequest(clientRequestObj);
					} else if (clientRequestObj instanceof LogoutRequest) {
						close(clientRequestObj);
					}
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}

		/**
		 * open
		 * @return boolean
		 * 
		 * @throws IOException
		 */
		private boolean open() throws IOException {
			if(clientSocket != null && !clientSocket.isClosed()){
				streamIn = new ObjectInputStream(clientSocket.getInputStream());
				return true;
			}else{
				return false;
			}
		}

		/**
		 * close
		 * 
		 * @throws IOException
		 */
		private void close(Object clientRequestObj) throws IOException,
				Exception {
			LogoutRequest logoutRequest = (LogoutRequest) clientRequestObj;
			LOG.info("Request received from client: "
					+ logoutRequest.toString());

			marketplaceServerApp.removeClient(logoutRequest.getUserId());
			
			if (clientSocket != null)
				clientSocket.close();
			if (streamIn != null)
				streamIn.close();

			
			LOG.info(logoutRequest.getUserId() + " logged out");
		}

		/**
		 * processLoginRequest
		 * 
		 * @param clientRequestObj
		 * @throws Exception
		 */
		private void processLoginRequest(Object clientRequestObj)
				throws Exception {
			LoginRequest loginRequest = (LoginRequest) clientRequestObj;
			LOG.info("Request received from client: "
					+ loginRequest.toString());

			int maxBuyQty = 5;
			int maxBuyPrice = 500;

			UserPermission userPermission = new UserPermission(
					loginRequest.getUserId(), maxBuyQty, maxBuyPrice);
			User user = new User(loginRequest.getUserId(), userPermission);

			marketplaceServerApp.addClient(user.getUserId(), this.clientSocket);
			
			boolean loginSuccessful = true;
			LoginResponse loginResponse = new LoginResponse(user,
					loginSuccessful, new ItemStatus(items), loginRequest);

			ObjectOutputStream oos = new ObjectOutputStream(
					clientSocket.getOutputStream());
			oos.writeObject(loginResponse);

			LOG.info("Login response sent to client: "
					+ loginResponse.toString());
		}

		/**
		 * processBuyRequest
		 * 
		 * @param clientRequestObj
		 * @throws Exception
		 */
		private void processBuyRequest(Object clientRequestObj)
				throws Exception {
			BuyRequest buyRequest = (BuyRequest) clientRequestObj;
			LOG.info("Buy received from client: "
					+ buyRequest.toString());

			BuyResponse buyResponse = marketplaceServerApp
					.processBuyRequest(buyRequest);
			ObjectOutputStream oos = new ObjectOutputStream(
					clientSocket.getOutputStream());
			oos.writeObject(buyResponse);

			LOG.info("Buy response sent to client: "
					+ buyResponse.toString());
			
			if(buyResponse.isBuyRequestSuccessful()){
				marketplaceServerApp.notifySeller(buyResponse);
			}
		}

		/**
		 * processSellRequest
		 * @param clientRequestObj
		 * @throws Exception
		 */
		private void processSellRequest(Object clientRequestObj)
				throws Exception {
			SellRequest sellRequest = (SellRequest) clientRequestObj;
			LOG.info("Sell request received from client: "
					+ sellRequest.toString());

			SellResponse sellResponse = marketplaceServerApp
					.processSellRequest(sellRequest);
			ObjectOutputStream oos = new ObjectOutputStream(
					clientSocket.getOutputStream());
			oos.writeObject(sellResponse);

			LOG.info("Sell response sent to client: "
					+ sellResponse.toString());
		}
	}

}
