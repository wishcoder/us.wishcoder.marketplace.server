/**
 * 
 */
package us.wishcoder.marketplace.server.messages;

import java.io.Serializable;

/**
 * @author ajaysingh
 *
 */
public class SellRequest implements Serializable {

	private static final long serialVersionUID = 4905601818439072303L;

	private final String userId;
	private final String itemName;
	private final int itemPrice;
	private final int itemQty;
	private final int itemMinPrice;
	
	/**
	 * @param userId
	 * @param itemName
	 * @param itemPrice
	 * @param itemQty
	 */
	public SellRequest(String userId, String itemName, int buyPrice, int buyQty, int itemMinPrice) {
		this.userId = userId;
		this.itemName = itemName;
		this.itemPrice = buyPrice;
		this.itemQty = buyQty;
		this.itemMinPrice = itemMinPrice;
	}
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}
	/**
	 * @return the itemPrice
	 */
	public int getItemPrice() {
		return itemPrice;
	}
	/**
	 * @return the itemQty
	 */
	public int getItemQty() {
		return itemQty;
	}
	/**
	 * @return the itemMinPrice
	 */
	public int getItemMinPrice() {
		return itemMinPrice;
	}

	@Override
	public String toString() {
		return "SellRequest [userId=" + userId + ", itemName=" + itemName
				+ ", itemPrice=" + itemPrice + ", itemQty=" + itemQty
				+ ", itemMinPrice=" + itemMinPrice + "]";
	}
}
