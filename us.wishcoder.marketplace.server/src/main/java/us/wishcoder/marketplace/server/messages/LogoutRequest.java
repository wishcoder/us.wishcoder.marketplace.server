/**
 * 
 */
package us.wishcoder.marketplace.server.messages;

import java.io.Serializable;

/**
 * @author ajaysingh
 *
 */
public class LogoutRequest implements Serializable {
	private static final long serialVersionUID = 4596112536927581834L;

	private final String userId;
	
	/**
	 * LogoutRequest
	 * @param userId
	 */
	public LogoutRequest(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LogoutRequest [userId=" + userId + "]";
	}
}
