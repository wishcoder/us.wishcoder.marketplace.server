/**
 * 
 */
package us.wishcoder.marketplace.server.messages;

import java.io.Serializable;



/**
 * @author ajaysingh
 *
 */
public class SellResponse implements Serializable {

	private static final long serialVersionUID = -1083853637989683590L;

	public static enum ERROR_CODE{
		ITEM_DOES_NOT_EXIST("Item doesn't exist"),
		EXCEPTIPON("Exception");
		
		private String value;
		ERROR_CODE(String value){
			this.value = value;
		}
		
		@Override
		public String toString(){
			return this.value;
		}
	}
	
	private final SellRequest sellRequest;
	private ItemStatus itemStatus;
	private boolean requestSuccessful;
	private ERROR_CODE errorCode;
	/**
	 * @param sellRequest
	 */
	public SellResponse(SellRequest sellRequest) {
		this.sellRequest = sellRequest;
	}
	/**
	 * @return the itemStatus
	 */
	public ItemStatus getItemStatus() {
		return itemStatus;
	}
	/**
	 * @param itemStatus the itemStatus to set
	 */
	public void setItemStatus(ItemStatus itemStatus) {
		this.itemStatus = itemStatus;
	}
	/**
	 * @return the requestSuccessful
	 */
	public boolean isRequestSuccessful() {
		return requestSuccessful;
	}
	/**
	 * @param requestSuccessful the requestSuccessful to set
	 */
	public void setRequestSuccessful(boolean requestSuccessful) {
		this.requestSuccessful = requestSuccessful;
	}
	/**
	 * @return the errorCode
	 */
	public ERROR_CODE getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(ERROR_CODE errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return the sellRequest
	 */
	public SellRequest getSellRequest() {
		return sellRequest;
	}

	@Override
	public String toString() {
		return "SellResponse [sellRequest=" + sellRequest
				+ ", requestSuccessful=" + requestSuccessful + ", errorCode="
				+ errorCode + "]";
	}
}
