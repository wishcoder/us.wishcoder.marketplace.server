/**
 * 
 */
package us.wishcoder.marketplace.server.messages;

import java.io.Serializable;

/**
 * @author ajaysingh
 *
 */
public class User implements Serializable {
	private static final long serialVersionUID = 8264453907050065276L;
	private final String userId;
	private final UserPermission userPermission;
	/**
	 * @param userId
	 * @param userPermission
	 */
	public User(String userId, UserPermission userPermission) {
		this.userId = userId;
		this.userPermission = userPermission;
	}
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @return the userPermission
	 */
	public UserPermission getUserPermission() {
		return userPermission;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userPermission=" + userPermission
				+ "]";
	}
}
