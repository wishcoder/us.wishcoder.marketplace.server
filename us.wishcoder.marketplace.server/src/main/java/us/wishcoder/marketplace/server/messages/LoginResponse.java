/**
 * 
 */
package us.wishcoder.marketplace.server.messages;

import java.io.Serializable;

/**
 * @author ajaysingh
 *
 */
public class LoginResponse implements Serializable {
	private static final long serialVersionUID = -4096495966497333408L;
	
	private final User user;
	private final boolean loginSuccessful;
	private final ItemStatus itemStatus;
	private final LoginRequest loginRequest;
	
	/**
	 * @param user
	 * @param loginSuccessful
	 * @param itemStatus
	 * @param loginRequest
	 */
	public LoginResponse(User user, boolean loginSuccessful,
			ItemStatus itemStatus, LoginRequest loginRequest) {
		this.user = user;
		this.loginSuccessful = loginSuccessful;
		this.itemStatus = itemStatus;
		this.loginRequest = loginRequest;
	}
	
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @return the loginSuccessful
	 */
	public boolean isLoginSuccessful() {
		return loginSuccessful;
	}

	/**
	 * @return the itemStatus
	 */
	public ItemStatus getItemStatus() {
		return itemStatus;
	}

	/**
	 * @return the loginRequest
	 */
	public LoginRequest getLoginRequest() {
		return loginRequest;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LoginResponse [user=" + user + ", loginSuccessful="
				+ loginSuccessful + ", loginRequest=" + loginRequest + "]";
	}
}
