/**
 * 
 */
package us.wishcoder.marketplace.server.messages;

import java.io.Serializable;



/**
 * @author ajaysingh
 *
 */
public class BuyResponse implements Serializable {
	private static final long serialVersionUID = 6417636680371383562L;

	public static enum ERROR_CODE{
		ITEM_DOES_NOT_EXIST("Item doesn't exist"),
		REQUIRE_MIN_BUY_PRICE("Require minnimum buy price"),
		ITEM_SOLD_OUT("Item sold out");
		
		private String value;
		ERROR_CODE(String value){
			this.value = value;
		}
		
		@Override
		public String toString(){
			return this.value;
		}
	}
	
	private final BuyRequest buyRequest;
	private ItemStatus itemStatus;
	private boolean buyRequestSuccessful;
	private Item buyItem;
	private ERROR_CODE errorCode;
	
	/**
	 * @param buyRequest
	 */
	public BuyResponse(BuyRequest buyRequest) {
		this.buyRequest = buyRequest;
	}

	/**
	 * @return the buyRequestSuccessful
	 */
	public boolean isBuyRequestSuccessful() {
		return buyRequestSuccessful;
	}

	/**
	 * @param buyRequestSuccessful the buyRequestSuccessful to set
	 */
	public void setBuyRequestSuccessful(boolean buyRequestSuccessful) {
		this.buyRequestSuccessful = buyRequestSuccessful;
	}

	/**
	 * @return the errorCode
	 */
	public ERROR_CODE getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(ERROR_CODE errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the buyRequest
	 */
	public BuyRequest getBuyRequest() {
		return buyRequest;
	}

	/**
	 * @param itemStatus the itemStatus to set
	 */
	public void setItemStatus(ItemStatus itemStatus) {
		this.itemStatus = itemStatus;
	}

	/**
	 * @return the itemStatus
	 */
	public ItemStatus getItemStatus() {
		return itemStatus;
	}

	
	/**
	 * @return the buyItem
	 */
	public Item getBuyItem() {
		return buyItem;
	}

	/**
	 * @param buyItem the buyItem to set
	 */
	public void setBuyItem(Item buyItem) {
		this.buyItem = buyItem;
	}

	@Override
	public String toString() {
		return "BuyResponse [buyRequest=" + buyRequest + ", buyRequestSuccessful="
				+ buyRequestSuccessful + ", errorCode=" + errorCode + "]";
	}
}
