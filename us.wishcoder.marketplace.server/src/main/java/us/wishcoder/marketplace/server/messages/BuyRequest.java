/**
 * 
 */
package us.wishcoder.marketplace.server.messages;

import java.io.Serializable;

/**
 * @author ajaysingh
 *
 */
public class BuyRequest implements Serializable {
	private static final long serialVersionUID = 1571764913480391263L;

	private final String userId;
	private final String itemName;
	private final int buyPrice;
	private final int buyQty;
	
	/**
	 * @param userId
	 * @param itemName
	 * @param buyPrice
	 * @param buyQty
	 */
	public BuyRequest(String userId, String itemName, int buyPrice, int buyQty) {
		this.userId = userId;
		this.itemName = itemName;
		this.buyPrice = buyPrice;
		this.buyQty = buyQty;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * @return the buyPrice
	 */
	public int getBuyPrice() {
		return buyPrice;
	}

	/**
	 * @return the buyQty
	 */
	public int getBuyQty() {
		return buyQty;
	}

	@Override
	public String toString() {
		return "BuyRequest [userId=" + userId + ", itemName=" + itemName
				+ ", buyPrice=" + buyPrice + ", buyQty=" + buyQty + "]";
	}
}
